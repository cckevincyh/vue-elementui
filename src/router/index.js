import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import SelectorComponent from '@/components/SelectorComponent'
import SelectorComponent2 from '@/components/SelectorComponent2'
import SelectorComponent3 from '@/components/SelectorComponent3'
import SelectorComponent4 from '@/components/SelectorComponent4'
import InputComponent from '@/components/InputComponent'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/test',
      name: 'SelectorComponent',
      component: SelectorComponent
    },
    {
      path: '/test2',
      name: 'SelectorComponent2',
      component: SelectorComponent2
    },
    {
      path: '/test3',
      name: 'SelectorComponent3',
      component: SelectorComponent3
    },
    {
      path: '/test4',
      name: 'SelectorComponent4',
      component: SelectorComponent4
    },
    {
      path: '/test5',
      name: 'InputComponent',
      component: InputComponent
    }
  ]
})
